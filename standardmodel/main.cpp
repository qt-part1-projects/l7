#include <QtGui>
#include <QApplication>
#include <QStandardItem>
#include <QTableView>

int main(int argc, char **argv){
    QApplication app(argc, argv);
    QTableView view;
    QStandardItemModel *model = new QStandardItemModel(&view);

    QStringList labels;
    labels.append("Name");
    labels.append("Deadline");
    labels.append("Completed (in %)");

    model->setHorizontalHeaderLabels(labels);

    QList<QStandardItem*> row;
    QStandardItem *item;

    item = new QStandardItem();
    item->setData(QString("do homework"),Qt::DisplayRole);
    item->setSelectable(false);
    row <<item;

    item = new QStandardItem();
    item->setData(QDate(2016,11,20),Qt::DisplayRole);
    item->setEditable(false);
    row <<item;

    item = new QStandardItem();
    item->setData(50,Qt::DisplayRole);
    row <<item;

    model->appendRow(row);

    view.setModel(model);

    view.show();
    return app.exec();
}
